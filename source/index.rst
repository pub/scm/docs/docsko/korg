Kernel.org Documentation
========================
.. important::
   This is a collection of documents for the users of kernel.org. If you
   are looking for Linux kernel documentation, you should head over to
   `docs.kernel.org <https://docs.kernel.org/>`_.

Git and related processes
-------------------------
.. toctree::
   :maxdepth: 1

   accounts
   access
   fido2
   gitolite/index
   gitolite/2fa
   gitolite/track_set
   gitolite/transparency-log
   nitrokey
   cgit-meta-data
   git-url-shorteners
   kup

Email and related
-----------------
.. toctree::
   :maxdepth: 1

   mail
   remail
   linuxdev
   lore

Other services
--------------
.. toctree::
   :maxdepth: 1

   wiki
   docs
   bugzilla
   patchwork/index
   patchwork/pwbot
   prtracker
   pgpkeys
   ksmap

Social accounts
---------------
.. toctree::
   :maxdepth: 1

   people
   social

Getting support
---------------
.. toctree::
   :maxdepth: 1

   support
