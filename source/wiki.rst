Requesting a new wiki site
==========================

.. important:: Do you really need a wiki?

   Back in the rosy days of the early Internet, wikis made perfect
   sense. Today, unless you're willing to constantly monitor your wiki
   for spam and abuse, you shouldn't really be asking to set up a wiki.
   What you probably want instead is a documentation site. Please review
   :doc:`docs`, and strongly consider if it's a better fit than having a
   wiki.

If you do have a convincing reason to have wiki site, we may be able to
host one for you. Please send the following information to
helpdesk@kernel.org:

- Name of the site: [foo.wiki.kernel.org]
- Purpose of the site: [Foo kernel module documentation]
- Reasons why it must be a wiki: [Migrating existing dokuwiki]
- Contact address for spam reports and complaints: [foo@example.com]

.. note::

   We only support new dokuwiki deployments.
